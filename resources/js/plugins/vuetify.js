// import '@fortawesome/fontawesome-free/css/all.css' // TODO fix
// import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

import Vuetify from 'vuetify/lib';

const opts = {
    theme: {
        themes: {
            light: {
                //design:
                primary: '#5468E7',
                primary_dark: '#38386B',
                accent: '#CF33C8',
                secondary: '#CACDEF',
                background: '#FEF9F4',
                background_accent: '#ffeed7',
                // default
                success: '#18BD5D',
                warning: '#FF7100',
                danger: '#EF0000',
                error: '#EF0000',
            },
        },
    },
    icons: {
        iconfont: 'mdi', // default - only for display purposes
    }
};

export default new Vuetify(opts);
