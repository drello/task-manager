import Vue from 'vue';
import axios from 'axios';
import router from '../router'

import auth                  from '@websanova/vue-auth/dist/v2/vue-auth.esm';
import driverAuthBearer      from '@websanova/vue-auth/dist/drivers/auth/bearer.esm.js';
import driverHttpAxios       from '@websanova/vue-auth/dist/drivers/http/axios.1.x.esm.js';
import driverRouterVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm.js';

Vue.use(auth, {
  plugins: {
    http: axios,
    router: router
  },
  drivers: {
    auth: driverAuthBearer,
    http: driverHttpAxios,
    router: driverRouterVueRouter,
  },
  options: {
    authRedirect: { name: 'auth.login' },
    notFoundRedirect: '/404',
    forbiddenRedirect: '/500',
    registerData: {
      url: '/api/register',
      method: 'POST',
      redirect: { name: 'projects.index' },
      fetchUser: true,
      autoLogin: false
    },
    loginData: {
      url: '/api/login',
      method: 'POST',
      redirect: { name: 'projects.index' },
      fetchUser: true
    },
    logoutData: {
      url: '/api/logout',
      method: 'GET',
      redirect: { name: 'auth.login' },
      makeRequest: true
    },
    fetchData: {
      url: '/api/user',
      method: 'GET',
      enabled: true
    },
    refreshData: {
      enabled: false,
    }
  }
});
