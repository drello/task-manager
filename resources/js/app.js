require('./bootstrap');
window.Vue = require('vue');

import Vue from 'vue';
import App from './layouts/App';
import router from "./router";
import vuetify from "./plugins/vuetify";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import './plugins/vue-auth';

Vue.use(Vuetify);
Vue.use(VueRouter);

const app = new Vue({
    el: '#app',
    router,
    vuetify,
    render: h => h(App)
});
