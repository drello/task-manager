import Router from 'vue-router';

const router = new Router({
  mode: 'history',
  history: true,
  routes: [
    {
      path: '/',
      name: 'projects.index',
      component: require('../views/projects/Index.vue').default,
      meta: {
        auth: true
      },
      props: true,
    },
    {
      path: '/projects/:hash/:slug',
      name: 'projects.show',
      component: require('../views/projects/Show.vue').default,
      meta: {
        auth: true
      },
      props: true,
    },
    {
      path: '/login',
      name: 'auth.login',
      component: require('../views/auth/LogIn.vue').default,
      meta: {
        auth: false
      },
      props: true
    },
    {
      path: '/sign-up',
      name: 'auth.sign-up',
      component: require('../views/auth/SignUp.vue').default,
      meta: {
        auth: false
      },
      props: true
    },
    {
      path: '/invite',
      name: 'invite',
      component: require('../views/invite/Invite.vue').default,
      props: true
    },
  ]
});

export default router;