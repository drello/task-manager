@component('mail::message')
# You're invited by the "{{ $projectName }}" project in **drello**

@component('mail::button', ['url' => $inviteLink])
Accept the invitation
@endcomponent

Thanks,<br>drello
@endcomponent