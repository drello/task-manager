<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\{
    ProjectController,
    BoardController,
    GroupController,
    CardController,
    AuthController,
    ShareController,
};

Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/user', [AuthController::class, 'user']);
    Route::get('/logout', [AuthController::class, 'logout']);
    Route::post('/invite', [ShareController::class, 'invite']);
    Route::post('/accept-invitation', [ShareController::class, 'acceptInvitation']);
    Route::resource('projects', ProjectController::class);
    Route::resource('projects/{projectId}/boards', BoardController::class);
    Route::resource('groups', GroupController::class);
    Route::resource('cards', CardController::class);
});
