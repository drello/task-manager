<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProjectResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'invited_token' => config('app.url') . "/invite?token=" . $this->invited_token . "&project_id=" . $this->id,
            'hash' => $this->hash,
            'slug' => $this->slug,
            'boards' => BoardResource::collection($this->boards),
            'users' => $this->users,
        ];
    }
}
