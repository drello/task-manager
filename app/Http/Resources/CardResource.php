<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CardResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'hash' => $this->hash,
            'name' => $this->name,
            'deadline' => $this->deadline,
            'created_at' => $this->created_at,
            'sprint' => $this->sprint,
            'description' => $this->description,
            'checkboxes' => $this->checkboxes,
            'file_path' => $this->file_path,
        ];
    }
}
