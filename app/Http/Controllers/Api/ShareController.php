<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\InviteMail;
use App\Models\Project;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ShareController extends Controller
{
    public function invite(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'project_id' => 'required|exists:projects,id',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }
        $project = Project::find($request->get('project_id'));
        $invitedToken = $project->invited_token;
        if (!$project->invited_token || $project->invited_token_expires_at && Carbon::now() >= Carbon::parse($project->invited_token_expires_at)) {
            $invitedToken = Str::random(20);
            $invitedTokenExpiresAt = Carbon::now()->addDay();
            $project->update([
                'invited_token' => $invitedToken,
                'invited_token_expires_at' => $invitedTokenExpiresAt,
            ]);
        }
        $inviteLink = config('app.url') . "/invite?token=" . $invitedToken . "&project_id=" . $project->id;
        try {
            Mail::to($request->get('email'))->send(new InviteMail($inviteLink, $project->name));
        }
        catch (\Exception $exception) {
            Log::error($exception->getMessage());
        }
        return response('', 204);
    }

    public function acceptInvitation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'project_id' => 'required|exists:projects,id',
            'token' => 'required|exists:projects,invited_token',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }
        $project = Project::query()->where([
            'id' => $request->get('project_id'),
            'invited_token' => $request->get('token'),
        ])->where('invited_token_expires_at', '>=', date('Y-m-d H:i:s'))->first();
        if ($project) {
            $ids = $project->users->pluck('id');
            $ids->add($request->get('user_id'));
            $ids = $ids->unique();
            $project->users()->sync($ids);
        }
        return response('', 204);
    }
}
