<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectResource;
use App\Models\Board;
use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProjectController extends Controller
{
    public function index()
    {
        $user = auth('sanctum')->user();
        return response()->json(ProjectResource::collection($user->projects));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['hash'] = Str::random(20);
        $data['slug'] = Str::slug($data['name']);
        $entity = Project::create($data);
        if ($request->get('user_id')) {
            $entity->users()->sync(collect([$request->get('user_id')]));
        }
        Board::create([
            'name' => Board::DEFAULT_NAME,
            'project_id' => $entity->id,
            'slug' => Str::slug(Board::DEFAULT_NAME),
            'hash' => Str::random(20),
        ]);
        return response()->json(new ProjectResource($entity), 201);
    }

    public function show(Request $request, $id)
    {
        $project = Project::where([
            'hash' => $request->get('hash'),
            'slug' => $request->get('slug'),
        ])->first();
        if (!$project->invited_token || $project->invited_token_expires_at && Carbon::now() >= Carbon::parse($project->invited_token_expires_at)) {
            $invitedToken = Str::random(20);
            $invitedTokenExpiresAt = Carbon::now()->addDay();
            $project->update([
                'invited_token' => $invitedToken,
                'invited_token_expires_at' => $invitedTokenExpiresAt,
            ]);
        }
        return response()->json(new ProjectResource($project));
    }

    public function update(Request $request, $id)
    {
        $entity = Project::find($id);
        $entity->update($request->all());
        return response()->json($entity, 200);
    }

    public function destroy($id)
    {
        $entity = Project::find($id);
        $entity->delete();
        return response()->json(null, 200);
    }
}
