<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CardResource;
use App\Models\Card;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CardController extends Controller
{
    public function index()
    {
        return response()->json(CardResource::collection(Card::all()));
    }

    public function store(Request $request)
    {
        $data = $request->all();
        $data['hash'] = Str::random(20);
        $entity = Card::create($data);
        return response()->json(new CardResource($entity), 201);
    }

    public function show(Request $request, $id)
    {
        $entity = Card::find($id);
        return response()->json(new CardResource($entity));
    }

    public function update(Request $request, $id)
    {
        $entity = Card::find($id);
        $entity->update($request->all());
        return response()->json($entity, 200);
    }

    public function destroy($id)
    {
        $entity = Card::find($id);
        $entity->delete();
        return response()->json(null, 200);
    }
}
