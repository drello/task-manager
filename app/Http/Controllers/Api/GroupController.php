<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\GroupResource;
use App\Models\Group;
use Illuminate\Http\Request;

class GroupController extends Controller
{
    public function index()
    {
        return response()->json(GroupResource::collection(Group::all()));
    }

    public function store(Request $request)
    {
        $entity = Group::create($request->all());
        return response()->json(new GroupResource($entity), 201);
    }

    public function show(Request $request, $id)
    {
        $entity = Group::find($id);
        return response()->json(new GroupResource($entity));
    }

    public function update(Request $request, $id)
    {
        $entity = Group::find($id);
        $entity->update($request->all());
        return response()->json($entity, 200);
    }

    public function destroy($id)
    {
        $entity = Group::find($id);
        $entity->delete();
        return response()->json(null, 200);
    }
}
