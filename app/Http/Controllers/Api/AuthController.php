<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users',
            'password' => 'required|min:6|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }

        $user = User::where(['email' => $request->get('email')])->first();
        if (Hash::check($request->get('password'), $user->password)) {

            if ($request->has('token') && $request->has('project_id')) {
                $project = Project::query()->where([
                    'id' => $request->get('project_id'),
                    'invited_token' => $request->get('token'),
                ])->where('invited_token_expires_at', '>=', date('Y-m-d H:i:s'))->first();
                if ($project) {
                    $ids = $project->users->pluck('id');
                    $ids->add($user->id);
                    $ids = $ids->unique();
                    $project->users()->sync($ids);
                }
            }

            $token = $user->createToken('app-token')->plainTextToken;
            return response()->json(['user' => $user], 201)->withHeaders(['Authorization' => $token]);
        }

        return response()->json([
            'errors' => [
                'password' => [
                    'The current password does not match this user.'
                ]
            ]
        ], 403);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|string'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ], 403);
        }

        $user = User::create($validator->validated());

        if ($request->has('token') && $request->has('project_id')) {
            $project = Project::query()->where([
                'id' => $request->get('project_id'),
                'invited_token' => $request->get('token'),
            ])->where('invited_token_expires_at', '>=', date('Y-m-d H:i:s'))->first();
            if ($project) {
                $ids = $project->users->pluck('id');
                $ids->add($user->id);
                $ids = $ids->unique();
                $project->users()->sync($ids);
            }
        }

        $token = $user->createToken('app-token')->plainTextToken;
        return response()->json(['user' => $user], 201)->withHeaders(['Authorization' => $token]);
    }

    public function logout(Request $request)
    {
        auth('sanctum')->user()->currentAccessToken()->delete();
        return response('', 204);
    }

    public function user(Request $request)
    {
        return response()->json(['data' => auth('sanctum')->user()], 201);
    }
}
