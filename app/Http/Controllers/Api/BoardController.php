<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Board;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BoardController extends Controller
{
    public function index(Request $request, $projectId)
    {
        $project = Project::find($projectId);
        return response()->json($project->boards);
    }

    public function store(Request $request, $projectId)
    {
        $project = Project::find($projectId);
        $data = $request->all();
        $data['project_id'] = $project->id;
        $data['hash'] = Str::random(20);
        $data['slug'] = Str::slug($data['name']);
        $entity = Board::create($data);
        return response()->json($entity, 201);
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $projectId, $id)
    {
        $entity = Board::find($id)->update($request->all());
        return response()->json($entity, 200);
    }

    public function destroy($projectId, $id)
    {
        $entity = Board::find($id);
        $entity->delete();
        return response()->json(null, 200);
    }
}
