<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InviteMail extends Mailable
{
    use Queueable, SerializesModels;

    private $inviteLink;
    private $projectName;

    public function __construct($inviteLink, $projectName)
    {
        $this->inviteLink = $inviteLink;
        $this->projectName = $projectName;
    }

    public function build()
    {
        return $this->subject("You're invited in drello!")
            ->markdown('emails.invite', [
                'inviteLink' => $this->inviteLink,
                'projectName' => $this->projectName,
            ]);
    }
}
