<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    use HasFactory;

    protected $fillable = [
        'group_id',
        'hash',
        'name',
        'deadline',
        'sprint',
        'description',
        'checkboxes',
        'file_path',
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }
}
