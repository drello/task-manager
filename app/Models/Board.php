<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Board extends Model
{
    use HasFactory;

    const DEFAULT_NAME = 'Default';

    protected $fillable = ['name', 'hash', 'slug', 'project_id'];

    public function groups()
    {
        return $this->hasMany(Group::class);
    }
}
