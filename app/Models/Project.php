<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'hash', 'slug',
        'invited_token', 'invited_token_expires_at',
    ];

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function boards()
    {
        return $this->hasMany(Board::class);
    }
}
