<?php

namespace Database\Factories;

use App\Models\Card;
use App\Models\Group;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Card::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->realText(20),
            'hash' => Str::random(20),
            'group_id' => $this->faker->randomElement(Group::all()->pluck('id')->toArray()),
            'deadline' => Carbon::tomorrow(),
            'sprint' => $this->faker->randomElement([1,2,3,4,5,6,7,8]),
            'description' => $this->faker->realText(50),
        ];
    }
}
