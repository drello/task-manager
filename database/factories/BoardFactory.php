<?php

namespace Database\Factories;

use App\Models\Board;
use App\Models\Project;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class BoardFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Board::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $name = $this->faker->realText(10);

        return [
            'name' => $name,
            'hash' => Str::random(20),
            'slug' => Str::slug($name),
            'project_id' => $this->faker->randomElement(Project::all()->pluck('id')->toArray()),
        ];
    }
}
