<?php

namespace Database\Seeders;

use App\Models\Board;
use App\Models\Card;
use App\Models\Group;
use App\Models\Project;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Project::factory(7)->create();
        Board::factory(14)->create();
        Group::factory(28)->create();
        Card::factory(54)->create();

        $users = [
            User::factory()->createOne(['email' => 'rudmira@example.com']),
            User::factory()->createOne(['email' => 'nikita@example.com']),
            User::factory()->createOne(['email' => 'anna@example.com']),
            User::factory()->createOne(['email' => 'igor@example.com']),
        ];

        $users[0]->projects()->attach([1, 2, 5]);
        $users[1]->projects()->attach([2, 4, 6]);
        $users[2]->projects()->attach([1, 3, 4]);
        $users[3]->projects()->attach([3, 7]);
    }
}
